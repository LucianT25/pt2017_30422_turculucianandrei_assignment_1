package jUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.polynomials.Polynomial;

public class MulTest {

	@Test
	public void test() {
		Polynomial p1 = new Polynomial("5x^4+3.25x^3+9x^2-3x+6");
		Polynomial p2 = new Polynomial("5x^3+2x^2-5.25x");
		Polynomial result = p1.multiplyBy(p2);		
		assertEquals("25x^7+26.25x^6+25.25x^5-14.062x^4-23.25x^3+27.75x^2-31.5x",result.toString());
	}

}
