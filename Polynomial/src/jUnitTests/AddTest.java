package jUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.polynomials.Polynomial;

public class AddTest {

	@Test
	public void test() {
		Polynomial p1 = new Polynomial("25x^4+3.25x^3+9x^2-33x+66");
		Polynomial p2 = new Polynomial("50x^3+20x^2-5.25x");
		Polynomial result = p1.addTo(p2);		
		assertEquals("25x^4+53.25x^3+29x^2-38.25x+66",result.toString());
	}

}
