package jUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.polynomials.Polynomial;

public class SubTest {

	@Test
	public void test() {
		Polynomial p1 = new Polynomial("25x^4+3.25x^3+9x^2-33x+66");
		Polynomial p2 = new Polynomial("50x^3+20x^2-5.25x");
		Polynomial result = p1.subtract(p2);		
		assertEquals("25x^4-46.75x^3-11x^2-27.75x+66",result.toString());
	}

}
