package controllers;

import java.awt.event.*;
import javax.swing.*; 
import entities.polynomials.Polynomial;

/**Turcu Lucian Andrei - group 30422*/
/**This is the main class, that starts the GUI and executes the program*/

public class Main extends JPanel {

public static void main(String[] args) {	
	JFrame frame = new JFrame ("Polynomial Operations");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setSize(500, 300);
	JPanel panel = new JPanel();
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel opPanel = new JPanel();
	JPanel resultPanel = new JPanel();
	panel.add(panel1);
	panel.add(panel2);
	panel.add(opPanel);
	panel.add(resultPanel);
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	frame.setContentPane(panel);
	frame.setVisible(true);
	
	JLabel l1 = new JLabel("First: ");
	JLabel l2 = new JLabel("Second:  ");
	JLabel l3 = new JLabel("Result: ");
	panel1.add(l1);
	panel2.add(l2);
	resultPanel.add(l3);
	
	JTextField tf1 = new JTextField("", 15);
	JTextField tf2 = new JTextField("", 15);
	JTextField tf3 = new JTextField("", 30);
	tf3.setEditable(false);	
	panel1.add(tf1);
	panel2.add(tf2);
	resultPanel.add(tf3);
	
	JButton intFirst = new JButton("Integrate");
	JButton intSec = new JButton("Integrate");
	JButton diffFirst = new JButton("Differentiate");
	JButton diffSec = new JButton("Differentiate");
	JButton add = new JButton("Add");
	JButton sub = new JButton("Subtract");
	JButton mul = new JButton("Multiply");
	JButton div = new JButton("Division");
	panel1.add(intFirst);
	panel1.add(diffFirst);
	panel2.add(intSec);
	panel2.add(diffSec);
	opPanel.add(add);
	opPanel.add(sub);
	opPanel.add(div);
	opPanel.add(mul);
	panel.revalidate();
	
	
	intFirst.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p1 = new Polynomial(tf1.getText());
			tf3.setText(p1.integrate().toString());			
		}
	});
	diffFirst.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p1 = new Polynomial(tf1.getText());
			tf3.setText(p1.differentiate().toString());
		}
	});
	intSec.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p2 = new Polynomial(tf2.getText());
			tf3.setText(p2.integrate().toString());
		}
	});
	diffSec.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p2 = new Polynomial(tf2.getText());
			tf3.setText(p2.differentiate().toString());
		}
	});
	add.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p1 = new Polynomial(tf1.getText());
			Polynomial p2 = new Polynomial(tf2.getText());
			tf3.setText(p1.addTo(p2).toString());
		}
	});
	sub.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p1 = new Polynomial(tf1.getText());
			Polynomial p2 = new Polynomial(tf2.getText());
			tf3.setText(p1.subtract(p2).toString());
		}
	});
	mul.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p1 = new Polynomial(tf1.getText());
			Polynomial p2 = new Polynomial(tf2.getText());
			tf3.setText(p1.multiplyBy(p2).toString());
		}
	});
	div.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Polynomial p1 = new Polynomial(tf1.getText());	
			if(tf2.getText().equals("")) {
				tf3.setText("Division by 0. Not ok!");
			}
			else {
				Polynomial p2 = new Polynomial(tf2.getText());
				tf3.setText(p1.divideBy(p2).toString());
			}
		}
	});
	return;
}
}
