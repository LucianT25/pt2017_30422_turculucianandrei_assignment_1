package entities.polynomials;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import comparators.PowerComparator;
import entities.monomials.Monomial;
import operations.Operations;

public class Polynomial implements Operations<Polynomial>{
private ArrayList<Monomial> monomials;
private int degree;

public Polynomial(int d) {
	this.degree = d;
	this.monomials = new ArrayList<Monomial>();
}

public Polynomial(String poly) {
	this.monomials = new ArrayList<Monomial>();
	poly = poly.replaceAll("\\*", "");
	poly = poly.replaceAll("\\s+", "");
	poly = poly.replaceAll("\\+-", "-");
	poly = poly.replaceAll("-", "+-");
	String[] members = poly.split("(\\+)");	
    for (String part : members) {
    	if(!(part.equals(""))){
    		float c = 0;
    		int pw = 0;
    		if(!(part.contains("x"))) { //if there's no x in the substring
    			c = Float.parseFloat(part);
    			pw = 0;
    		}
    		else { //if there's an x in the substring
    			String[] temp = part.split("x");
    			if(temp.length == 2) {   //contains coeff and power
    				temp[1] = temp[1].replaceAll("\\^", "");
    				pw = Integer.parseInt(temp[1]);
    				if(temp[0].equals("-")) {
    					c = -1;
    				}
    				else {
    					c = Float.parseFloat(temp[0]);
    				}
    			}
    			else if(temp.length == 1) {
    				if(temp[0].contains("^")) {
    					temp[0] = temp[0].replaceAll("//^", "");
    					c = 0;
    					pw = Integer.parseInt(temp[0]);
    				}
    				else {
    					if(temp[0].equals("-")) {
        					c = -1;
        				}
        				else {
        					c = Float.parseFloat(temp[0]);
        				}
    					pw = 1;
    				}
    			}
    			else if(temp.length == 0) {
    				c = 1;
    				pw = 1;
    			}
    		}
    		if(c != 0) {
    			Monomial newMember = new Monomial(pw, c);
        		this.insertMonomial(newMember);
    		}
    		
    	}
    }	
}

public void insertMonomial(Monomial m) {
	int i = 0;
	for(Monomial c: monomials) {
		
		if(c.getPower() == m.getPower()) {
			monomials.set(i, c.addTo(m));
			return;
		}
		i++;
	}			
	if(monomials.isEmpty() || m.getPower() > monomials.get(0).getPower()) {
		this.degree = m.getPower();
	}
	
	this.monomials.add(m);
	
	Collections.sort(monomials, Collections.reverseOrder(new PowerComparator()));
	return;
}

public void removeMonomial(int p) {
	for(Monomial m: monomials) {
		if (m.getPower() == p) {
			monomials.remove(m);
			this.degree = this.monomials.get(0).getPower();
			return;
		}
	}
	return;	
}

public int getDegree() {
	return this.degree;
}

public ArrayList<Monomial> getMonomials() {
	return this.monomials;
}

public boolean isEqual(Polynomial p) {
	if(this.degree == p.getDegree()) {		
		for(int i = 0; i < this.monomials.size(); i++) {
			if(this.monomials.get(i).getCoeff() != p.getMonomials().get(i).getCoeff()) {
				return false;
			}
		}
		return true;
	}
	else {
		return false;
	}
}

//Operations:
@Override
public Polynomial addTo(Polynomial o) {
	Polynomial result = new Polynomial(0);
	for (Monomial m1: this.monomials) {
		result.insertMonomial(m1);
	}
	for (Monomial m2: o.getMonomials()) {
		result.insertMonomial(m2);
	}
	return result;
}

@Override
public Polynomial subtract(Polynomial o) {	
	Polynomial result = new Polynomial(0);
	if(this.isEqual(o) == true) {
		return result;
	}
	
	for(Monomial m1: this.monomials) {
		result.insertMonomial(m1);
	}
	
	for(Monomial m2: o.getMonomials()) {
		int match = 0;
		int i = 0;
		for(Monomial r: result.getMonomials()) {
			if(m2.getPower() == r.getPower()) {
				result.getMonomials().set(i, r.subtract(m2));
				if(result.getMonomials().get(i).getCoeff() == 0) {
					result.removeMonomial(result.getMonomials().get(i).getPower());
				}
				match = 1;
				break;
			}		
			i++;
		}
		if(match == 0)
			result.insertMonomial(new Monomial(m2.getPower(), 0-m2.getCoeff()));		
	}
	return result;
}


@Override
public Polynomial multiplyBy(Polynomial o) {
	Polynomial result = new Polynomial(0);
	for(Monomial m1: this.monomials) {
		for(Monomial m2: o.getMonomials()) {
			result.insertMonomial(m1.multiplyBy(m2));
		}
	}
	return result;
}

@Override
public Polynomial divideBy(Polynomial o) {
	if(o.getMonomials().isEmpty()) {
		return null;
	}
	Polynomial answer = new Polynomial(0);
	Polynomial remainder = new Polynomial(0);
	if(this.getDegree() < o.getDegree()) {
		answer.insertMonomial(new Monomial(0,0));
		remainder = this;
		return answer;
	}
	else {
		for(Monomial m: this.getMonomials()){
			remainder.insertMonomial(m);
		}
		while(remainder.getDegree() >= o.getDegree()) {
			Monomial div = remainder.getMonomials().get(0).divideBy(o.getMonomials().get(0));
			answer.insertMonomial(div);
			Polynomial divPol = new Polynomial(0);
			divPol.insertMonomial(div);
			remainder = remainder.subtract(o.multiplyBy(divPol));	
		}	
	}
	return answer;
}

@Override
public Polynomial integrate() {
	Polynomial result = new Polynomial(0);
	for(Monomial m: this.monomials) {
		result.insertMonomial(m.integrate());
	}
	return result;
}

@Override
public Polynomial differentiate() {
	Polynomial result = new Polynomial(0);
	for(Monomial m: this.monomials) {
		result.insertMonomial(m.differentiate());
	}
	return result;
}

public String toString() {
	if(this.degree == 0 && this.monomials.isEmpty()) {
		return "0";
	}
	StringBuilder sb = new StringBuilder();
	DecimalFormat f = new DecimalFormat();
    f.setDecimalSeparatorAlwaysShown(false);
	int c = 1;
	
	for(Monomial m: this.getMonomials()) {
		if(m.getCoeff() >= 0 && c != 1) {
			sb.append("+");
		}		
		sb.append(m.toString());		
		c++;
	}
	return sb.toString();
}


}
